import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { Storage } from '@ionic/storage';

import {Home} from "../home/home";
import {LoginPage} from "../login/login";

import * as Parse from 'parse';
/*
  Generated class for the Intro page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-intro',
  templateUrl: 'intro.html'
})
export class IntroPage {


  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public storage: Storage
  ) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad IntroPage');
  }

  goToHome(){
    //this.navCtrl.setRoot(Page1);
    // Start Parse User
    let user = Parse.User.current();
    console.log(` User => ${user}`);

    this.storage.set('introShown', true);

    if (!user) {
      console.log(` setRoot(LoginPage) `);
      this.navCtrl.setRoot(LoginPage);
    } else {
      console.log(` setRoot(Page1) `);
      this.navCtrl.setRoot(Home);
    }
  }

}
