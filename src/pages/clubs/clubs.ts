import { Component } from '@angular/core';

import { NavController, NavParams } from 'ionic-angular';

//PROVIDERS
import { Clubs } from '../../providers/clubs';

@Component({
  selector: 'page-clubs',
  templateUrl: 'clubs.html'
})
export class ClubsPage {
  selectedItem: any;
  icons: string[];
  items: Array<{title: string, note: string, icon: string}>;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private _clubs: Clubs
  ) {

    this.items = [];

    console.log("HELLO ClubsPage");
    console.log(this._clubs.clubs);
    this.items = [...this._clubs.clubs];

    this._clubs.subjectclubs.subscribe((val) => {
      console.log("ClubsPage _CLUBS.SUBSCRIBE ");
      console.log(val);
      this.items = [...val];
    });


    // If we navigated to this page, we will have an item available as a nav param
    this.selectedItem = navParams.get('item');

    // Let's populate this page with some filler content for funzies
    this.icons = ['flask', 'wifi', 'beer', 'football', 'basketball', 'paper-plane',
    'american-football', 'boat', 'bluetooth', 'build'];

    /*
    for (let i = 1; i < 11; i++) {
      this.items.push({
        title: 'Item ' + i,
        note: 'This is item #' + i,
        icon: this.icons[Math.floor(Math.random() * this.icons.length)]
      });
    }
    */
  }

  itemTapped(event, item) {
    // That's right, we're pushing to ourselves!
    this.navCtrl.push(ClubsPage, {
      item: item
    });
  }
}
