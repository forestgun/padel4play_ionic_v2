import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';


import { Contactos } from '../../providers/contactos';
import { Clubs } from '../../providers/clubs';
import { Grupocontactos } from '../../providers/grupocontactos';
import { Partidotipos } from '../../providers/partidotipos';
import { Partidos } from '../../providers/partidos';
import {Meapuntolive} from "../../providers/meapuntolive";
import {AddPartidoPage} from "../add-partido/add-partido";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class Home {

  tipolista: string = "mispartidos";
  contactos: any[];
  clubs: any[];
  grupocontactos: any[];
  partidotipos: any[];
  partidos: any[];
  meapuntos: any[];

  constructor(public navCtrl: NavController,
              private _contactos: Contactos,
              private _clubs: Clubs,
              private _grupocontactos: Grupocontactos,
              private _partidotipos: Partidotipos,
              private _partidos: Partidos,
              private _meapuntos: Meapuntolive
  ) {

    console.log("Entramos en HOME");
    this.contactos = [];
    this.clubs = [];
    this.grupocontactos = [];
    this.partidotipos = [];
    this.partidos = [];
    this.meapuntos = [];
    /*
    this._contactos.getContacts().subscribe(
      contactos => {
        console.log("subscribe getContacts() event");
        console.log(contactos);
        this.contactos = contactos;
      },
      error => {
        console.log("error subscribe getContacts()");
        console.log(error);
      }, ()=> {
        console.log("completed subscribe getContacts()");
      }
    )
    */
    this._partidos.subjectpartidos.subscribe((val) => {
      console.log("HomePage _PARTIDOS.SUBSCRIBE ");
      console.log(val);
      this.partidos = [...val];
      console.log(this.partidos);
    });
    this._meapuntos.subjectmeapuntolive.subscribe((val)=>{
      console.log("HomePage _MEAPUNTOLIVE.SUBSCRIBE");
      console.log(val);
      this.meapuntos = [...val];
    });

    this._contactos.subjectcontactos.subscribe((val) => {
      console.log("HomePage _CONTACTOS.SUBSCRIBE ");
      console.log(val);
      this.contactos = [...val];
    });
    this._clubs.subjectclubs.subscribe((val) => {
      console.log("HomePage _CLUBS.SUBSCRIBE ");
      console.log(val);
      this.clubs = [...val];
    });
    this._grupocontactos.subjectgrupocontactos.subscribe((val) => {
      console.log("HomePage _GRUPOCONTACTOS.SUBSCRIBE ");
      console.log(val);
      this.grupocontactos = [...val];
    });
    this._partidotipos.subjectpartidotipos.subscribe((val) => {
      console.log("HomePage _PARTIDOTIPOS.SUBSCRIBE ");
      console.log(val);
      this.partidotipos = [...val];
    });


  }


  addPartido(){
    this.navCtrl.push(AddPartidoPage);
  }


}
