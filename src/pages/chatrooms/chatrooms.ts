import {Component, ViewChild} from "@angular/core";
import {NavController, ModalController, Events, Content} from "ionic-angular";
import {ChatFormPage} from "../chat-form/chat-form";
import {ChatroomsProvider} from "../../providers/chatrooms";
import {ChatmessagesPage} from "../chatmessages/chatmessages";
import _ from "underscore";
import {AnalyticsProvider} from "../../providers/analytics";

import {PARSE_APP_ID, PARSE_JS_KEY, PARSE_SERVER_URL, GOOGLE_ANALYTICS} from "../../config";

//declare const Parse: any;
import * as Parse from 'parse';


@Component({
  selector   : 'page-chatrooms',
  templateUrl: 'chatrooms.html'
})
export class ChatroomsPage {

  @ViewChild('Content') content: Content;

  errorIcon: string      = 'chatbubbles';
  errorText: string      = '';
  data                   = [];
  loading: boolean       = true;
  showEmptyView: boolean = false;
  showErrorView: boolean = false;
  moreItem: boolean      = false;

  params = {
    limit: 20,
    page : 1
  };

  constructor(public navCtrl: NavController,
              private provider: ChatroomsProvider,
              private modalCtrl: ModalController,
              private events: Events,
              private analytics: AnalyticsProvider,
  ) {
    console.log('Hello ChatroomsPage Page');
    Parse.initialize(PARSE_APP_ID, PARSE_JS_KEY);
    Parse.serverURL = PARSE_SERVER_URL;
    // Google Analytics
    this.analytics.view('ChatChannel page');

    this.events.subscribe('channel:update', () => this.find());
  }

  ionViewDidLoad() {
    console.log("Chatrooms Page ionViewDidLoad");
    this.find();
  }

  onPageMessage(item) {
    this.navCtrl.push(ChatmessagesPage, {channel: item.id});
  }


  parseResult(data) {
    console.log(data);
    if (data) {
      let user = Parse.User.current();
      data.map(channel => {
        channel.users = _.filter(channel.users, _user => user.id != _user['id']);
        this.data.push(channel);
      });
      this.showEmptyView = false;
      this.showErrorView = false;
    } else {
      this.moreItem = false;
    }

    if (this.data.length < 1) {
      this.showEmptyView = true;
    }

    this.loading = false;
    return data;
  }

  find() {
    console.log("Chatrooms Page - find()");
    return new Promise((resolve, reject) => {
      this.loading = true;
      this.data    = [];
      this.provider.find().then(data => {
        this.parseResult(data);
        resolve(data);
      }, error => {
        this.showErrorView = true;
        reject(error);
      });
    });
  }


  public scrollTop() {
    this.content.scrollToTop();
  }

  public doInfinite(event) {
    this.params.page++;
    this.find().then(() => event.complete());
  }

  public doRefresh(event?) {
    this.params.page = 1;
    this.find().then(() => event.complete());
  }


  onModalChatForm() {
    this.modalCtrl.create(ChatFormPage).present();
  }

}
