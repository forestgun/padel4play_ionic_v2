import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';


import { UserProvider } from "../../providers/user";
import {IonicUtilProvider} from "../../providers/ionic-util";

// PAGES
import { IntroPage } from '../intro/intro';



/*
  Generated class for the Perfil page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-perfil',
  templateUrl: 'perfil.html'
})
export class PerfilPage {
  options: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private util: IonicUtilProvider,
              private provider: UserProvider
  ) {

    this.options = {
      background: {
        url: "/assets/PELOTAS.jpg",
        classes: "your-custom-optional-background-class",
        blur: {
          value:'5px',
          colors: {
            start: 'rgba(43, 40, 50, 0.8)',
            mid:'rgba(83, 86, 99, 0.8)',
            end:'rgba(69, 77, 91, 0.6)'
          }
        }
      },
      img: {
        url: "/assets/profile.jpg",
        classes: "your-custom-optional-img-class"
      },
      name: {
        text: "Kylee Parker",
        classes: 'your-custom-optional-name-class'
      },
      subtext: {
        text: "Ryderwood, GA",
        classes: 'your-custom-optional-subtext-class'
      }
    }


  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PerfilPage');
  }


  onLogout(): void {
    console.log('PerfilPage onLogout() ');
    //console.log(` form => ${form}`);
    //if (form.valid) {
    this.util.onLoading();

    this.provider.logout();

      this.util.endLoading();
      this.navCtrl.setRoot(IntroPage);

    //}
  }


}
