import { Component } from '@angular/core';
import {Validators, FormBuilder} from "@angular/forms";
import { NavController, NavParams } from 'ionic-angular';

import {Home} from "../home/home";
import { AppComponent  } from '../../app/app.component';

import { UserProvider } from "../../providers/user";
import {IonicUtilProvider} from "../../providers/ionic-util";

import { Masterdata } from '../../providers/masterdata';


/*
  Generated class for the Login page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/

declare const Parse: any;

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {

  formLogin: any;

  //public username: string;
  //public password: string;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private formBuilder: FormBuilder,
              private util: IonicUtilProvider,
              private provider: UserProvider,
              private masterdata: Masterdata
  ) {

  }

  ionViewWillLoad() {
    console.log('ionViewWillLoad LoginPage');
    this.formLogin = this.formBuilder.group({
      username: ['', Validators.compose([Validators.required, Validators.minLength(4)])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(5)])],
    });
    console.log(` this.formLogin => ${this.formLogin}`);

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
    let current = this.provider.current();
    console.log(` Current() => ${current}`);
  }

  onLogin(form): void {
    console.log('LoginPage onLogin() ');
    console.log(` form => ${form}`);
    //if (form.valid) {
      this.util.onLoading();

      this.provider.signIn(this.formLogin.value).then(user => {
        console.log("LoginPage => onLogin()");
        console.log(user);
        console.log(this.provider.current());
        this.masterdata.getMasterData(user);
        this.util.endLoading();
        this.navCtrl.setRoot(Home);
      }).catch(error => {
        console.log(error);
        this.util.endLoading();
        this.util.toast(error.message);
      });
    //}
  }

  onRegister() {
    console.log('LoginPage onRegister()');
  }
}
