import { Component, ChangeDetectorRef } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';

import { PartidoModel } from '../../models/partido.model';
import { TablaPistasComponent } from '../../components/tabla-pistas/tabla-pistas';

//PROVIDERS
import { Clubs } from '../../providers/clubs';
import { Partidotipos } from '../../providers/partidotipos';
import { UserProvider } from '../../providers/user';

import {PARSE_APP_ID, PARSE_JS_KEY, PARSE_SERVER_URL, GOOGLE_ANALYTICS} from "../../config";

import * as Parse from 'parse';
import * as moment from 'moment';

/*
  Generated class for the AddPartido page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-add-partido',
  templateUrl: 'add-partido.html'
})
export class AddPartidoPage {

  tipopartido: string = "privado";
  datetimePartido: string;
  partido: PartidoModel;

  pistas: any[];
  availability: any;

  clubs: any[];
  patidotipos: any[];

  clubseleccionado: any;
  partidotipo: any;
  currentUser: any;
  timeValue: any;

  duracionPartido: any;
  tabla_availability: any[];
  showhorasdisponibles: boolean;
  onGuardar: boolean = false;
  showselecthorapista: boolean = false;
  showconfirmacionreserva: boolean = false;
  showconfirmacionreserva_boton: boolean = false;
  showreservaconfirmada: boolean = false;

  loading: any;
  spinner: any;

  selecthorapista: any;

  idpista: string;
  namepista: string;
  horaselected: string;

  idreservation: string;
  reserva: any;



  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private _clubs: Clubs,
              private _patidotipos: Partidotipos,
              private _userprovider: UserProvider,
              private ref: ChangeDetectorRef,
              public loadingCtrl: LoadingController,
              public alertCtrl: AlertController
  ) {
    console.log("Hello AddPartidoPage");
    Parse.initialize(PARSE_APP_ID, PARSE_JS_KEY);
    Parse.serverURL = PARSE_SERVER_URL;
    this.clubseleccionado = '';
    this.pistas = [];
    this.duracionPartido = {hours:1,minutes:30} ;
    this.availability = {data: null, params: null};
    this.currentUser = this._userprovider.current();
    this.datetimePartido = new Date().toISOString();
    this.tabla_availability = [];
    this.showhorasdisponibles = false;
    this.showselecthorapista = false;
    this.selecthorapista = {};

    this.idpista = '';
    this.namepista = '';
    this.horaselected = '';

    this.idreservation = '';
    this.reserva = {};

    this.loading = this.loadingCtrl.create({
      content: 'Por favor espere...'
    });

    this.spinner = this.loadingCtrl.create({
      content: ''
    });

    this._clubs.subjectclubs.subscribe((val) => {
      console.log("AddPartidoPage _CLUBS.SUBSCRIBE ");
      console.log(val);
      this.clubs = [...val];
    });
    this._patidotipos.subjectpartidotipos.subscribe((val) => {
      console.log("AddPartidoPage _patidotipos.SUBSCRIBE ");
      console.log(val);
      this.patidotipos = [...val];
    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddPartidoPage');
  }

  showAlert(title: string, subtitle: string) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: subtitle,
      buttons: [{
        text: 'OK',
        handler: () => {

          let navTransition = alert.dismiss();
          navTransition.then(()=>{
            this.navCtrl.pop();
          });
          return false;
        }
      }]
    });
    alert.present();
  }

  selectClubFecha(fechaseleccionada: any){
    this.showselecthorapista = false;
    this.showconfirmacionreserva = false;
    this.showconfirmacionreserva_boton = false;
    this.showreservaconfirmada = false;

    this.loading.present();

    console.log(fechaseleccionada);
    let solofecha = moment(fechaseleccionada);
    let solohorario = fechaseleccionada;
    console.log("clubseleccionado.id");
    console.log(this.clubseleccionado);
    console.log(this.clubseleccionado.objectId);
    let clubid = this.clubseleccionado.objectId;
    console.log("currentUser.id");
    console.log(this.currentUser);
    console.log(this.currentUser.id);
    let userid = this.currentUser.id;

    console.log("Pedidos newsession - padelclick");
    Parse.Cloud.run("newsession",{ClubId: clubid, UserId: userid}).then( (data) =>{
      console.log(data);
      console.log(data.sessionId);
      let sessionkey = data.sessionId;
      let resources = {};
      console.log(solofecha);
      let solofechaparse = solofecha.date()+'-'+(solofecha.month()+1)+'-'+solofecha.year();
      console.log(solofechaparse);

      console.log("Pedimos resources - padelclick");
      Parse.Cloud.run("resources",{ClubId: clubid, UserId: userid}).then( (data) =>{
        let resources = data;
        this.pistas = [...resources];
        console.log(resources);

        console.log("Pedimos availability - padelclick");
        Parse.Cloud.run("availability", {ClubId: clubid, UserId: userid, date: solofechaparse}).then( (data) =>{
            this.availability.data = data;
            this.availability.params = data.params;
            console.log("Tenemos availability");
            console.log(this.availability);
           // let temp_availability = this.availability.data.map(this.filtrarHoras);
            let temp_availability = this.availability.data.map((value, index) => {
              //console.log(` Index = ${index}`);
              //console.log(`Value => `);
              //console.log(value);
              let mod = this.filtrarHoras(value, index);
              //console.log(mod);
              return mod;
            });
            console.log("temp_availability");
            console.log(temp_availability);
            let temp_list_not_null = temp_availability.filter((value)=>{
              //console.log(value);
              //console.log(value ? true : false);
              return value ? true : false ;
            });
            console.log(temp_list_not_null);
            this.tabla_availability = temp_list_not_null;
            this.showhorasdisponibles = true;
            this.loading.dismiss();
            /* data => array de  resources con availability
             {
             end: "17/04/2017 10:30",
             idResource: 1477,
             resourceName: "Pista Autogal-BMW",
             start: "17/04/2017 10:00"
             }
             */
            /* TODO -- CONVERTIR EL ARRAY AVAILABILITY EN UNA MATRIZ PARA MOSTAR COMO UNA TABLA
                O OTRO FORMATO DE VISUALIZAR DE FORMA ORDENADA PISTAS POR HORAS
            */
        },
          (error)=>{
            console.log("Error pidiendo availibility");
            console.log(error);
            this.loading.dismiss();

          });

      },
        (error)=>{
          console.log("Error pidiendo resources");
          console.log(error);
          this.loading.dismiss();

        });

    },
      (error) => {
      console.log("Error generando newsession");
      console.log(error);
        this.loading.dismiss();

      });

  }


  filtrarHoras (item,index){
    let nuevorowItem: any = {};
    let startItem = moment.utc(item.start, 'DD/MM/YYYY HH:mm');
    let endItem = moment.utc(item.end, 'DD/MM/YYYY HH:mm');
    let gameDateTime = moment.utc(this.datetimePartido);
    let min_gameDateTime = gameDateTime.clone();
    let max_gameDateTime = gameDateTime.clone();
    let end_gameDateTime = gameDateTime.clone();

    let hora = {};
    let i = 0;

    min_gameDateTime.subtract(1.5,'h');
    max_gameDateTime.add(0.5,'h');
    let endmax_gameDateTime = max_gameDateTime.clone();
    end_gameDateTime.add(this.duracionPartido);
    endmax_gameDateTime.add(this.duracionPartido);

    //console.log("datetimePartido => ");
    //console.log(this.datetimePartido);
    //console.log(gameDateTime);
    //console.log('FiltrarHoras => '+ index +' partido: '+ gameDateTime.format("DD/MM/YYYY HH:mm") +' start: '+ startItem.format("DD/MM/YYYY HH:mm") +' end: '+ endItem.format("DD/MM/YYYY HH:mm") +'___'+ gameDateTime.isBetween(startItem,endItem) );

    //console.log('min_gameDateTime = '+ min_gameDateTime.format("DD/MM/YYYY HH:mm")+' max_gameDateTime = '+ max_gameDateTime.format("DD/MM/YYYY HH:mm") +' end_gameDateTime = '+ end_gameDateTime.format("DD/MM/YYYY HH:mm") +' endmax_gameDateTime = '+ endmax_gameDateTime.format("DD/MM/YYYY HH:mm") );


    //if (gameDateTime.isBetween(startItem,endItem) && end_gameDateTime.isBetween(startItem,endItem) ){
    if ( (gameDateTime.isSame(startItem) || gameDateTime.isAfter(startItem) )  && (end_gameDateTime.isSame(endItem) || end_gameDateTime.isBefore(endItem) )   ){

      //console.log('FiltrarHoras => '+ index +' AQUI PUEDE RESERVAR' );
      nuevorowItem.idResource = item.idResource;
      nuevorowItem.resourceName = item.resourceName;
      nuevorowItem.horas = [];

      // generamos horas desde 1h30m antes del inicio del partido
      // en tramos de 30 minutos hasta la hora de inicio del partido
      if (gameDateTime.isSame(startItem)){
        //console.log("gameDateTime.isSame(startItem) - TRUE");
        for ( i = 0; i <= 2; i++) {
          hora = {id: i, hora: min_gameDateTime.format('HH:mm'), isvalid: false};
          nuevorowItem.horas.push(hora);
          min_gameDateTime.add(0.5,'h');
        }
        hora = {id: 3, hora: gameDateTime.format('HH:mm'), isvalid: true};
        nuevorowItem.horas.push(hora);

      } else {
        //console.log("gameDateTime.isSame(startItem) - FALSE");
        //console.log(i);
        for ( i = 0; i <= 2; i++) {
          //console.log(startItem.format("DD/MM/YYYY HH:mm"));
          //console.log(startItem);
          //console.log(min_gameDateTime.format("DD/MM/YYYY HH:mm"));
          //console.log(min_gameDateTime);
          if (startItem.isAfter(min_gameDateTime)){
            //console.log("startItem.isAfter(min_gameDateTime) -- TRUE isvalid=false");
            hora = {id: i, hora: min_gameDateTime.format('HH:mm'), isvalid: false};
            nuevorowItem.horas.push(hora);
            min_gameDateTime.add(0.5,'h');
          } else {
            //console.log("startItem.isAfter(min_gameDateTime) -- FALSE isvalid=true");
            hora = {id: i, hora: min_gameDateTime.format('HH:mm'), isvalid: true};
            nuevorowItem.horas.push(hora);
            min_gameDateTime.add(0.5,'h');
          }

        }
        hora = {id: 3, hora: gameDateTime.format('HH:mm'), isvalid: true};
        nuevorowItem.horas.push(hora);

      }

      // generamos horas desde 30m despues del inicio del partido
      // en tramos de 30 minutos hasta el final del partido
      if (end_gameDateTime.isSame(endItem)){
        //console.log("gameDateTime.isSame(endItem) - TRUE");

        for ( i = 4; i <= 6; i++) {
          hora = {id: i, hora: max_gameDateTime.format('HH:mm'), isvalid: false};
          nuevorowItem.horas.push(hora);
          max_gameDateTime.add(0.5,'h');
        }

      } else {
        //console.log("gameDateTime.isSame(endItem) - FALSE");
        //console.log(i);
        for ( i = 4; i <= 6; i++) {
          //console.log(endItem.format("DD/MM/YYYY HH:mm"));
          //console.log(endItem);
          //console.log(endmax_gameDateTime.format("DD/MM/YYYY HH:mm"));
          //console.log(endmax_gameDateTime);
          if (endItem.isBefore(endmax_gameDateTime)) {
            //console.log("endItem.isBefore(endmax_gameDateTime) -- TRUE isvalid=false");
            hora = {id: i, hora: max_gameDateTime.format('HH:mm'), isvalid: false};
            nuevorowItem.horas.push(hora);
            max_gameDateTime.add(0.5, 'h');
            endmax_gameDateTime.add(0.5, 'h');
          } else {
            //console.log("endItem.isBefore(endmax_gameDateTime) -- FALSE isvalid=true");
            hora = {id: i, hora: max_gameDateTime.format('HH:mm'), isvalid: true};
            nuevorowItem.horas.push(hora);
            max_gameDateTime.add(0.5, 'h');
            endmax_gameDateTime.add(0.5, 'h');
          }

        }
      }

      return nuevorowItem;

    } else {
      /* TODO -- gameDateTime ni coincide con inicio de tramo y se encuentra dentro del tramo */
      /* TODO -- aqui se deberia evaluar si empezando mas tarde el partido , se encuentra un tramo */
      /* TODO -- valido y añadirlo

          EJEMPLO DEL PROBLEMA:
          - BUSCO RESERVA PARA LAS 21:00 PERO NO EXISTE PISTA , EN CAMBIO
            TENEMOS 6 PISTAS LIBRES DESDE LAS 21:30 HASTA LAS 23:00 ....
            EL RESULTADO DE LA BUSQUEDA ES QUE NO EXISTEN PISTAS DISPONIBLES

       */
      /*
       nuevorowItem.idResource = '';
       nuevorowItem.resourceName = '';
       nuevorowItem.horas = [];
       */
      return;

    }



  }

  selectHoraYPista(event):void{
    console.log("Evento recibido en selectHoraYPista");
    console.log(event);
    this.selecthorapista = event;
    this.showhorasdisponibles = false;
    this.showselecthorapista = true;
    this.spinner.present();
    /*
      event.idresource
      event.hora
      event.resourcename
     */
    console.log("clubseleccionado.id");
    //console.log(this.clubseleccionado);
    console.log(this.clubseleccionado.objectId);
    let clubid = this.clubseleccionado.objectId;
    console.log("currentUser.id");
    //console.log(this.currentUser);
    console.log(this.currentUser.id);
    let userid = this.currentUser.id;

    this.idpista = event.idresource;
    this.namepista = event.resourcename;
    this.horaselected = event.hora;

    let hora = this.horaselected;


    let minutes = 90; // TODO esto tiene que ser un select en la vista.
    let fecha = moment.utc(this.datetimePartido);
    let temp_hora = hora.split(":");
    fecha.hour(parseInt(temp_hora[0]));
    fecha.minute(parseInt(temp_hora[1]));
    let date = fecha.date()+'-'+(fecha.month()+1)+'-'+fecha.year();
    let start = moment(date, "DD-MM-YYYY").format("DD-MM-YYYY")+'%20'+hora;  // start=3-8-2012%2018:00
    console.log('AddPartidoPage - newprereservation -> start => '+ JSON.stringify(start) +' minutos => '+ JSON.stringify(minutes) +' -- ');


    Parse.Cloud.run("newprereservation", { UserId: userid, ClubId: clubid, start: start, minutes: minutes, idresource: this.idpista } ).then( (data)=> {
      //padelclickFactory.newprereservation(data, config).then(function(data) {

      console.log('AddPartidoPage - padelclickFactory.newprereservation() -> data => ' + JSON.stringify(data) + ' -- ');
      //var customerid = data.data.id;
      if (data.result == "OK"){
        console.log('result == OK');
        this.idreservation = data.idReservation;
        this.reserva = {};
        //$scope.reserva.idReservation = data.data.idReservation;
        //$scope.reserva.idReservation = data.data.idReservation;
        for (let key in data) {
          if (data.hasOwnProperty(key)) {
            console.log(key + ': ' + data[key]);
            this.reserva[key] = data[key];
          }
        }
        console.log('Fin for key data');
        /*
         {"result":"OK","idReservation":36312,"idResource":1479,"idResourceType":56,"idresourceSubtype":108,"resourceName":"Pista Central Stores Persan ","timeout":"19/09/2016 01:01","timeoutGmt":"2016-09-19 01:01:09 GMT+02:00","total":12,"allowPaymentPending":true,"paymentMethods":[]}
         */

        this.reserva.result = undefined;
        //$scope.sessionID = sessionkey; Ya no tenemos sessiones aqui esta en el servidor

        this.showconfirmacionreserva = true;
        this.showconfirmacionreserva_boton = true;
        this.spinner.dismiss();

        console.log(this.reserva);
        console.log('Lanzamos this.ref.detectChanges() para refrescar vista componente');
        this.ref.detectChanges();
        //$scope.$apply(); // forzamos que se lance un $digest para escribir el resultado en la vista por si se demora CloudCode



      } else {
        console.log(data);
        if (data.error){
          this.showhorasdisponibles = false;
          this.showselecthorapista = false;
          this.spinner.dismiss();
          this.ref.detectChanges();
          //alert('Error en reserva'+ JSON.stringify(data.error) );
          this.showAlert('Error en pre-reserva', data.error);
        }
      }

    }, (error) => {
      this.spinner.dismiss();
      alert('error padelclickFactory.newprereservation()' + JSON.stringify(error));
    });



  }

  confirmarysave(partido,player) {
    console.log("clubseleccionado.id");
    //console.log(this.clubseleccionado);

    this.showconfirmacionreserva_boton = false;
    this.spinner.present();

    console.log(this.clubseleccionado.objectId);
    let clubid = this.clubseleccionado.objectId;
    console.log("currentUser.id");
    //console.log(this.currentUser);
    console.log(this.currentUser.id);
    let userid = this.currentUser.id;
    Parse.Cloud.run("confirmreservation", { UserId: userid, ClubId: clubid , idreservation: this.idreservation}).then( (data) => {
      console.log('AddPartidoPage - confirmreservation() -> data => ' + JSON.stringify(data) + ' -- ');
      //var customerid = data.data.id;
      if (data.result == "OK"){
        this.showreservaconfirmada = true;
        this.spinner.dismiss();

        this.ref.detectChanges();
        alert('OK reserva CONFIRMADA' );
/*
        TODO -- GRABAR PARTIDO EN PARSE
        partidosFactory.addpartido(partido,players).then(function(data){
          //console.log('MiagendaCtrl - partidosFactory -> data => '+ JSON.stringify(data) +' -- ');
          $timeout(function() {
            $state.go('app.miagenda');
          }, 1000);

        }, function(error){
          alert('error partidosFactory' + JSON.stringify(error));
        });
*/
      } else {
        this.spinner.dismiss();
        alert('ERROR RESERVA NO VALIDA');
        // TODO -- SE TENDIAR QUE BORRAR EL PARTIDO DE PARSE-SERVE
      }

    }, (error) => {
      this.spinner.dismiss();
      // TODO -- SE TENDIAR QUE BORRAR EL PARTIDO DE PARSE-SERVE
      alert('error padelclickFactory.confirmreservation()' + JSON.stringify(error));
    });
  }

  savePartido(){

  }


  pullrelation(value){

  }

  pullContactosInvitados(todosInvitados,grupos_checked){

  }


}

