/* tslint:disable:member-ordering */
import { Directive, ElementRef, Input, OnInit} from '@angular/core';

@Directive({
  selector: '[sidemenu]'
})
export class SideMenuDirective implements OnInit{

  constructor(private elementRef: ElementRef) {

  }

  ngOnInit(){
    this.applyStyle();
  }

  @Input('sidemenu') sideMenuStyle: string;

  applyStyle=function(){if(this.elementRef.nativeElement.className=this.elementRef.nativeElement.className+" "+this.sideMenuStyle,"sidemenu-glass-avatar"==this.sideMenuStyle){var e=this.elementRef.nativeElement.querySelector("img");e.onload=function(){var t=".sidemenu-glass-avatar::before {background: url('"+e.src+"');}",l=document.createElement("div");l.innerHTML="<style>"+t+"</style>",document.getElementsByTagName("head")[0].appendChild(l)}}};

}
