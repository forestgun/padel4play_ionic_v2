import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import {HttpModule, Http} from '@angular/http';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Device } from '@ionic-native/device';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import {GoogleAnalytics} from "@ionic-native/google-analytics";

//import { Storage } from '@ionic/storage';
import { IonicStorageModule } from '@ionic/storage';

//import { TranslateModule } from 'ng2-translate/ng2-translate';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

import {MomentModule} from 'angular2-moment';
import 'moment/locale/es';

//MODELS
import { GrupocontactosModel } from '../models/grupocontactos.model';


//PROVIDERS
import {IonicUtilProvider} from "../providers/ionic-util";
import {UserProvider} from "../providers/user";
import {ParsePushProvider} from "../providers/parse-push";
import {ParseFileProvider} from "../providers/parse-file";
import {AnalyticsProvider} from "../providers/analytics";
import { ChatMessageProvider } from '../providers/chat-message';
import { ChatroomsProvider  } from '../providers/chatrooms';

import { Clubs } from '../providers/clubs';
import { Contactos } from '../providers/contactos';
import { Grupocontactos } from '../providers/grupocontactos';
import { Meapuntolive } from '../providers/meapuntolive';
import { Padelclick } from '../providers/padelclick';
import { Partidos } from '../providers/partidos';
import { Partidoslive } from '../providers/partidoslive';
import { Partidotipos } from '../providers/partidotipos';
import { Userclubs } from '../providers/userclubs';
import { Masterdata } from '../providers/masterdata';


//PAGES
import { AppComponent } from './app.component';
import { Home } from '../pages/home/home';
import { ClubsPage } from '../pages/clubs/clubs';
import {IntroPage} from "../pages/intro/intro";
import {LoginPage} from "../pages/login/login";
import {RegisterPage} from "../pages/register/register";
import { PerfilPage } from '../pages/perfil/perfil';
import {AddPartidoPage} from "../pages/add-partido/add-partido";

import { ChatmessagesPage } from '../pages/chatmessages/chatmessages';
import { ChatroomsPage } from '../pages/chatrooms/chatrooms';
import { ChatFormPage } from '../pages/chat-form/chat-form';

//DIRECTIVES
import { ProfileDirective } from './profile.directive';
import { SideMenuDirective } from './sidemenu.directive';

//COMPONENTS
import { IonProfileHeader } from '../components/ion-profile-header'
import {LoaderComponent} from '../components/loader/loader';
import { TablaPistasComponent} from '../components/tabla-pistas/tabla-pistas';

import * as Parse from 'parse';
import * as PouchDB from "pouchdb";


// AoT requires an exported function for factories
export function HttpLoaderFactory(http: Http) {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}


@NgModule({
  declarations: [
    AppComponent,
    Home,
    ClubsPage,
    IntroPage,
    LoginPage,
    RegisterPage,
    PerfilPage,
    AddPartidoPage,
    ProfileDirective,
    SideMenuDirective,
    IonProfileHeader,
    LoaderComponent,
    ChatmessagesPage,
    ChatroomsPage,
    ChatFormPage,
    TablaPistasComponent

  ],
  imports: [
    IonicModule.forRoot(AppComponent),
    IonicStorageModule.forRoot({
      name: '__padel4play',
         driverOrder: ['indexeddb', 'sqlite', 'websql']
    }),
    TranslateModule.forRoot({
              loader: {
                  provide: TranslateLoader,
                  useFactory: HttpLoaderFactory,
                  deps: [Http]
              },
            isolate: true
    }),
    BrowserModule,
    HttpModule,
    MomentModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    AppComponent,
    Home,
    ClubsPage,
    IntroPage,
    LoginPage,
    RegisterPage,
    PerfilPage,
    AddPartidoPage,
    ChatmessagesPage,
    ChatroomsPage,
    ChatFormPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    InAppBrowser,
    Device,
    GoogleAnalytics,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    IonicUtilProvider,
    ParsePushProvider,
    ParseFileProvider,
    AnalyticsProvider,
    UserProvider,
    ChatMessageProvider,
    ChatroomsProvider,
    Clubs,
    Contactos,
    Grupocontactos,
    Meapuntolive,
    Padelclick,
    Partidos,
    Partidoslive,
    Partidotipos,
    Userclubs,
    GrupocontactosModel,
    Masterdata

  ]
})
export class AppModule {}
