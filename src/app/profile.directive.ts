/* tslint:disable:member-ordering */
import { Directive, ElementRef, Input, OnInit} from '@angular/core';

@Directive({
  selector: '[profile]'
})
export class ProfileDirective implements OnInit{

  constructor(private elementRef: ElementRef) {

  }

  ngOnInit(){
    this.applyStyle();
  }

  @Input('profile') profileStyle: string;

  applyStyle=function(){if(this.elementRef.nativeElement.className=this.elementRef.nativeElement.className+" "+this.profileStyle,"profile-small-glass"==this.profileStyle){var e=this.elementRef.nativeElement.querySelector("img");e.onload=function(){var l=".profile-small-glass::before {background: url('"+e.src+"');}",t=document.createElement("div");t.innerHTML="<style>"+l+"</style>",document.getElementsByTagName("head")[0].appendChild(t)}}if("profile-large-glass"==this.profileStyle){var e=this.elementRef.nativeElement.querySelector("img");e.onload=function(){var l=".profile-large-glass::before {background: url('"+e.src+"');}",t=document.createElement("div");t.innerHTML="<style>"+l+"</style>",document.getElementsByTagName("head")[0].appendChild(t)}}};
}
