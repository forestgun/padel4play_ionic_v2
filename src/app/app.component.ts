import { Component, ViewChild, OnInit } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Device } from '@ionic-native/device';

import { LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { ProfileDirective } from './profile.directive';
import { SideMenuDirective } from './sidemenu.directive';

import {PARSE_APP_ID, PARSE_JS_KEY, PARSE_SERVER_URL, GOOGLE_ANALYTICS} from "../config";




//providers
import {ParsePushProvider} from "../providers/parse-push";
import {AnalyticsProvider} from "../providers/analytics";

import { Clubs } from '../providers/clubs';
import { Contactos } from '../providers/contactos';
import { Grupocontactos } from '../providers/grupocontactos';
import { Meapuntolive } from '../providers/meapuntolive';
import { Padelclick } from '../providers/padelclick';
import { Partidos } from '../providers/partidos';
import { Partidoslive } from '../providers/partidoslive';
import { Partidotipos } from '../providers/partidotipos';
import { Userclubs } from '../providers/userclubs';


import { Masterdata } from '../providers/masterdata';


//pages
import { Home } from '../pages/home/home';
import { ClubsPage } from '../pages/clubs/clubs';
import { IntroPage } from '../pages/intro/intro';
//import {LoginPage} from "../pages/login/login";
//import {RegisterPage} from "../pages/register/register";
import { PerfilPage } from '../pages/perfil/perfil';
import { ChatroomsPage } from "../pages/chatrooms/chatrooms";


//declare const Parse: any;
import * as Parse from 'parse';

@Component({
  templateUrl: 'app.html',

})

export class AppComponent implements OnInit {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = Home;
  loader: any;

  public usercurrent: any;

  // pages have additional information like the current page indicator and the icon to be used
  pages: Array<{title: string, component: any, icon: string, indicator: string}>;

  profile: {img: string, name: string};

  ngOnInit() {
    Parse.initialize(PARSE_APP_ID, PARSE_JS_KEY);
    Parse.serverURL = PARSE_SERVER_URL;
  }

  constructor(public platform: Platform,
              private statusBar: StatusBar,
              private device: Device,
              private splashscreen: SplashScreen,
              private Push: ParsePushProvider,
              private Analytics: AnalyticsProvider,
              public loadingCtrl: LoadingController,
              public storage: Storage,
              private clubs: Clubs,
              private contactos: Contactos,
              private grupocontactos: Grupocontactos,
              private partidos: Partidos,
              private partidotipos: Partidotipos,
              private meapuntolive: Meapuntolive,
              private partidoslive: Partidoslive,
              private masterdata: Masterdata

  ) {
    console.log('Hello AppComponent');

    //this.presentLoading();

    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: Home, icon: 'icon ion-ios-home-outline', indicator: 'current-indicator' },
      { title: 'Chats', component: ChatroomsPage, icon: 'icon ion-ios-chatbubbles-outline', indicator: '' },
      { title: 'Clubs', component: ClubsPage, icon: 'icon ion-ios-tennisball-outline', indicator: '' },
      { title: 'Perfil', component: PerfilPage, icon: 'icon ion-ios-contact-outline', indicator: '' }
    ];
    // set the profile profile pic and name
    this.profile = { img: "assets/profile.jpg", name: 'Someone' };

  }

  initializeApp() {
    console.log("Device.platform before platform.ready() => ", this.device.platform);
    console.log("Device.cordova before platform.ready() => ", this.device.cordova);
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashscreen.hide();

      console.log("Device.platform after platform.ready() => ", this.device.platform);
      console.log("Device.cordova after platform.ready() => ", this.device.cordova);

      // Google Analytics
      this.Analytics.init(GOOGLE_ANALYTICS);
      this.Analytics.appVersion(this.device.version);

      // Start Parse User
      let user = Parse.User.current();
      console.log(` User => ${user}`);
      console.log(user);
      this.usercurrent = user;

      this.loader = this.loadingCtrl.create({
        content: "Authenticating..."
      });

      if (!user) {
        // No Logueado
        console.log("no estamos logueados");
        console.log(user);
        this.rootPage = IntroPage;
      } else {
        // Si Logueado
        console.log("si estamos logueados");
        console.log(user);
        this.Push.init();
        // Cargar datos Maestros
        console.log("llamamos a carga de datos maestros");

        this.masterdata.getMasterData(this.usercurrent);

        this.onPartidosLive();

        this.onMeapuntoLive();

        //this.rootPage = TabsPage;
        this.storage.get('introShown').then((result) => {
          console.log(` introShown => ${result} `);
          if(result){
            this.loader.dismiss();
            this.rootPage = Home;
          } else {
            this.loader.dismiss();
            this.rootPage = IntroPage;
            this.storage.set('introShown', true);
          }
          this.loader.dismiss();
        });
      }



    });
  }


  onPartidosLive() {
    // Activamos Live Query en Partidos
    // Observable u Observador ¿?
    console.log("onPartidosLive");
    let _partidoslive = this.partidoslive;
    console.log(_partidoslive);
  }

  onMeapuntoLive() {
    // Activamos Live Query en Meapunto (son partidos que pueden interesar segun query)
    // Observable u Observador ¿?
    console.log("onMeapuntoLive");
    let _meapuntolive = this.meapuntolive;
    console.log(_meapuntolive);



  }


  presentLoading() {

    this.loader.present();

  }

  resetIndicator() {
    // loop through allt he pages, then reset the indicator
    var x;
    for (x=0; x < this.pages.length; x++) {
      this.pages[x].indicator = '';
    }
  }

  openPage(page) {
    // remove all indicators
    this.resetIndicator();
    // set the indicator to the selected page
    page.indicator = 'current-indicator';
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
