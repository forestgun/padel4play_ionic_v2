import {Observable} from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

export class GrupocontactosModel {

  grupocontactos: any;
  grupocontactosObserver: any;


  public grupo: any;
  public contactos: any[];


  constructor() {
    this.grupo = {};
    this.contactos = [];
/*
    this.grupocontactos = Observable.create(observer => {
      this.grupocontactosObserver = observer;
    });
*/
    this.grupocontactosObserver = new BehaviorSubject<any>(false);

  }



  public addContactos(items): void {
    //console.log("addContactos => items");
    //console.log(items);
    /*
    for (let item of items) {
      //console.log(item);
      this.contactos.push(item);
      //console.log("tihs.contactos.push(item)");
      //console.log(this.contactos);
    }
    */
    let _contactos = [...this.contactos,...items];
    this.contactos = _contactos;
    this.grupocontactosObserver.next(true);
  }

  public removeContacto(item): void {
    let _contactos = this.contactos;
    for(let i = 0; i < _contactos.length; i++) {
      if(_contactos[i] == item){
        _contactos.splice(i, 1);
      }
    }
    this.contactos = _contactos;
    this.grupocontactosObserver.next(true);
  }

  public setGrupo(grupo): void {
    //console.log("setGrupo => grupo");
    //console.log(grupo);
    this.grupo = grupo;
    this.grupocontactosObserver.next(true);
  }

}
