export interface PartidoModel {
  id?: string,
  club: any,
  reserva: any,
  pista: any,
  owner: any,
  fecha: any,
  owner_nick: any,
  nivelpartido: any,
  plazas: any,
  players: any,
  jugador_1: any,
  jugador_2: any,
  jugador_3: any,
  jugador_4: any,

}
