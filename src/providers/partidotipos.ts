import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
//import 'rxjs/add/operator/map';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import {PARSE_APP_ID, PARSE_JS_KEY, PARSE_SERVER_URL, GOOGLE_ANALYTICS} from "../config";

import * as Parse from 'parse';

/*
  Generated class for the Partidotipos provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class Partidotipos {

  // public estadospedidos: Array<Estadopedido>;
  public partidotipos: Array<any>;
  private _partidotipos: Array<any>;

  public subjectpartidotipos = new BehaviorSubject(null);

  constructor(public http: Http) {
    console.log('Hello Partidotipos Provider');
    Parse.initialize(PARSE_APP_ID, PARSE_JS_KEY);
    Parse.serverURL = PARSE_SERVER_URL;

    this.partidotipos = [];  // usar en formularios
    this._partidotipos = [];

    this.subjectpartidotipos.next(this.partidotipos);

    /*
    // PROMISE -- CLUBS
    this.getallpartidotipos().then((result) => {
      console.log("getallpartidotipos resuelve con result => ");
      //console.log(result);
      let jsonfile = JSON.stringify(result);
      let object = JSON.parse(jsonfile);
      //console.log(object);
      for (let partidotipo of object) {
        //this.estadospedidos.push(new Estadopedido(estado.objectId, estado.nombre));
        this.partidotipos.push(partidotipo);
      }
      console.log(this.partidotipos);
    }, (error) => {
      console.log("getallpartidotipos resuelve con error => ");
      console.error(error);
    });
    */
  }


  // GET -- CLUBS
  public getallpartidotipos(){
    return new Promise((resolve, reject) => {
      let partidotipoParse = Parse.Object.extend("PartidoTipo");
      let partidotiposparse = new Parse.Query(partidotipoParse);
      //tipopedidoparse.equalTo();
      partidotiposparse.find( {
        success: function(partidotiposparse) {
          // Execute any logic that should take place after the object is saved.
          console.log(partidotiposparse);
          this._partidotipos = [...partidotiposparse];
          this.partidotipos = this._partidotipos;
          resolve(this.partidotipos);
        },
        error: function( error) {
          // Execute any logic that should take place if the save fails.
          // error is a Parse.Error with an error code and message.
          console.log("PartidotiposProvider - getallpartidotipos() ERROR");
          console.log(error);
          //alert('Failed to create new object, with error code: ' + error.message);
          reject(error);
        }
      });
    });

  }

}
