import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
//import 'rxjs/add/operator/map';
//import {Observable} from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';


import {PARSE_APP_ID, PARSE_JS_KEY, PARSE_SERVER_URL, GOOGLE_ANALYTICS} from "../config";

import * as Parse from 'parse';

/*
  Generated class for the Clubs provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class Clubs {

  // public estadospedidos: Array<Estadopedido>;
  public clubs: Array<any>;
  private _clubs: Array<any>;

  public subjectclubs = new BehaviorSubject(null);

  constructor(public http: Http) {
    console.log('Hello Clubs Provider');
    Parse.initialize(PARSE_APP_ID, PARSE_JS_KEY);
    Parse.serverURL = PARSE_SERVER_URL;

    this.clubs = [];  // usar en formularios
    this._clubs = [];

    this.subjectclubs.next(this.clubs);

    // PROMISE -- CLUBS
    /*
    this.getallclubs().then((result) => {
      console.log("getallclubs resuelve con result => ");
      //console.log(result);
      let jsonfile = JSON.stringify(result);
      let object = JSON.parse(jsonfile);
      //console.log(object);
      for (let club of object) {
        //this.estadospedidos.push(new Estadopedido(estado.objectId, estado.nombre));
        this._clubs.push(club);
      }
      console.log(this._clubs);
    }, (error) => {
      console.log("getallclubs resuelve con error => ");
      console.error(error);
    });
    */
  }


  // GET -- CLUBS
  public getallclubs(){
    return new Promise((resolve, reject) => {
      console.log("ClubsProvider - getallclubs()");
      let clubParse = Parse.Object.extend("Club");
      let clubparse = new Parse.Query(clubParse);
      //tipopedidoparse.equalTo();
      clubparse.find( {
        success: function(clubparse) {
          // Execute any logic that should take place after the object is saved.
          console.log("getallclubs() => result");
          console.log(clubparse);
          this._clubs = [...clubparse];
          this.clubs = this._clubs;
          console.log("getallclubs() => resolve");
          console.log(this.clubs);
          //this.subjectclubs.next(clubparse);
          console.log("getallclubs() => salimos");
          resolve(this.clubs);
        },
        error: function( error) {
          // Execute any logic that should take place if the save fails.
          // error is a Parse.Error with an error code and message.
          console.log("ClubsProvider - getallclubs() ERROR");
          console.log(error);
          //alert('Failed to create new object, with error code: ' + error.message);
          reject(error);
        }
      });
    });

  }

  /*
  public getClubs(){
    return Observable.of(this.clubs);
  }
  */

}
