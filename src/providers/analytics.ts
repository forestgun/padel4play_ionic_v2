import {Injectable} from "@angular/core";
import {GoogleAnalytics} from "@ionic-native/google-analytics";
import {Platform} from "ionic-angular";

@Injectable()
export class AnalyticsProvider {
    cordova:boolean = false;
    constructor(private platform: Platform,
                private googleanalytics: GoogleAnalytics
    
    ) {
        this.cordova = platform.is('cordova') ? true : false;
    }

    init(GOOGLE_ID:string) {
        if(this.cordova) {
            this.googleanalytics.startTrackerWithId(GOOGLE_ID);
        }
    }

    view(name: string) {
        if(this.cordova) {
            this.googleanalytics.trackView(name);
        }
    }

    event(category: string, action: string) {
        if(this.cordova) {
            this.googleanalytics.trackEvent(category, action);
        }
    }

    appVersion(version:string){
        if(this.cordova) {
            this.googleanalytics.setAppVersion(version);
        }
    }
}
