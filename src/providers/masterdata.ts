import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
 Generated class for the Padelclick provider.

 See https://angular.io/docs/ts/latest/guide/dependency-injection.html
 for more info on providers and Angular 2 DI.
 */

//providers
import { Clubs } from './clubs';
import { Contactos } from './contactos';
import { Grupocontactos } from './grupocontactos';
import { Meapuntolive } from './meapuntolive';
import { Padelclick } from './padelclick';
import { Partidos } from './partidos';
import { Partidoslive } from './partidoslive';
import { Partidotipos } from './partidotipos';
import { Userclubs } from './userclubs';



@Injectable()
export class Masterdata {

  public usercurrent: any;

  constructor(public http: Http,
              private clubs: Clubs,
              private contactos: Contactos,
              private grupocontactos: Grupocontactos,
              private partidos: Partidos,
              private partidotipos: Partidotipos,
              private meapuntolive: Meapuntolive,
              private partidoslive: Partidoslive
  ) {
    console.log('Hello Masterdata Provider');
  }

  public getMasterData(usercurrent): void {
    // Hacemos llamadas a backend para coger los datos maestros (datos iniciales para la app)
    console.log(' Masterdata.getMasterData(usercurrent)');
    this.usercurrent = usercurrent;

    //get clubs
    this.clubs.getallclubs().then((allclubs) => {
      console.log("Masterdata => getMasterData() => this.clubs.getallclubs()");
      console.log(allclubs);
      let jsonfile = JSON.stringify(allclubs);
      let object = JSON.parse(jsonfile);
      this.clubs.clubs = object;
      console.log("Masterdata => getMasterData() => this.clubs.clubs");
      console.log(this.clubs.clubs);
      this.clubs.subjectclubs.next(this.clubs.clubs);
      console.log(" Masterdata => Salimos de getallclubs()");

    }, (error) => {
      console.log("Masterdata => getMasterData() => this.clubs.getallclubs() - ERROR");
      console.log(error);
    });


    //get partido tipos
    this.partidotipos.getallpartidotipos().then((allpartidotipos) => {
      console.log("Masterdata => getMasterData() => this.partidotipos.getallpartidotipos()");
      console.log(allpartidotipos);
      let jsonfile = JSON.stringify(allpartidotipos);
      let object = JSON.parse(jsonfile);
      this.partidotipos.partidotipos = object;
      console.log("Masterdata => getMasterData() => this.partidotipos.partidotipos");
      console.log(this.partidotipos.partidotipos);
      this.partidotipos.subjectpartidotipos.next(this.partidotipos.partidotipos);
      console.log(" Masterdata => Salimos de getallpartidotipos()");

    }, (error) => {
      console.log("Masterdata => getMasterData() => this.partidotipos.getallpartidotipos() - ERROR");
      console.log(error);
    });

    //get contactos
    this.contactos.getallmycontactos(this.usercurrent).then((allmycontactos) => {
      console.log("Masterdata => getMasterData() => this.contactos.getallmycontactos()");
      console.log(allmycontactos);
      let jsonfile = JSON.stringify(allmycontactos);
      let object = JSON.parse(jsonfile);
      this.contactos.contactos = object;
      console.log("Masterdata => getMasterData() => this.contactos.contactos");
      console.log(this.contactos.contactos);
      this.contactos.subjectcontactos.next(this.contactos.contactos);
      console.log(" Masterdata => Salimos de getallmycontactos()");

    }, (error) => {
      console.log("Masterdata => getMasterData() => this.contactos.getallmycontactos() - ERROR");
      console.log(error);
    });

    //get gruposcontactos
    this.grupocontactos.getallmygruposcontactos(this.usercurrent).then((allmygruposcontactos) => {
      console.log("Masterdata => getMasterData() => this.grupocontactos.getallmygruposcontactos()");
      console.log(allmygruposcontactos);
      let jsonfile = JSON.stringify(allmygruposcontactos);
      let object = JSON.parse(jsonfile);
      this.grupocontactos.gruposcontactos = object;
      this.grupocontactos.subjectgrupocontactos.next(this.grupocontactos.gruposcontactos);
      console.log(" Masterdata => Salimos de getallmygruposcontactos()");

    }, (error) => {
      console.log("Masterdata => getMasterData() => this.grupocontactos.getallmygruposcontactos() - ERROR");
      console.log(error);
    });

    //get userclubs

    //get partidos
    // Despues de retornar de get partidos se llama a onPartidosLive() y onMeapuntoLive()
    this.partidos.getallmypartidos(this.usercurrent).then((allmypartidos) => {
      console.log("Masterdata => getMasterData() => this.partidos.getallmypartidos()");
      console.log(allmypartidos);
      let jsonfile = JSON.stringify(allmypartidos);
      let object = JSON.parse(jsonfile);
      this.partidos.partidos = object;
      this.partidos.subjectpartidos.next(this.partidos.partidos);
      console.log(" Masterdata => Salimos de getallmypartidos()");

    }, (error) => {
      console.log("Masterdata => getMasterData() => this.partidos.getallmypartidos() - ERROR");
      console.log(error);
    });

    /*
      LA PRIMERA VEZ QUE ENTRAMOS EN LA APP O CUANDO NO ESTAMOS LOGUEADOS ,
      LAS LIVEQUERY NO SE CREAN (SE CARGA LOS PROVIDERS PERO NO SE INICIAN LAS QUERY
      POR NO DISPONER DE SESSIONTOKEN.
      TODO -- TENEMOS QUE AQUI COMPROBAR SI ESTAMOS LOGUEADOS Y TENEMOS SESSIONTOKEN
      ENTONCES LLAMAMOS AL METODO DE LAS LIVEQUERY PROVIDERS => *.getsessionToken()
     */

    this.meapuntolive.getsessionToken();

    this.partidoslive.getsessionToken();



  }

}
