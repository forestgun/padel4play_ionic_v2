import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
//import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Rx';

import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { UserProvider } from './user';

import {PARSE_APP_ID, PARSE_JS_KEY, PARSE_SERVER_URL, GOOGLE_ANALYTICS} from "../config";

import * as Parse from 'parse';
/*
  Generated class for the Meapuntolive provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class Meapuntolive {

  public meapuntolive: Array<any>;
  private _meapuntolive: Array<any>;

  client: any;
  LiveQueryClient: any;
  parseSession: any;
  subscription: any;
  query: any;


  public subjectmeapuntolive = new BehaviorSubject(null);


  constructor(public http: Http,
              private userprovider: UserProvider
  ) {
    console.log('Hello Meapuntolive Provider');
    Parse.initialize(PARSE_APP_ID, PARSE_JS_KEY);
    Parse.serverURL = PARSE_SERVER_URL;

    this.meapuntolive = [];  // usar en formularios
    this._meapuntolive = [];

    this.parseSession = null;
    this.subscription = null;
    this.query = null;

    this.subjectmeapuntolive.next(this.meapuntolive);

    this.getsessionToken();

  }


  // GET -- PARTIDOS
  /*
  public getallmeapuntolive(usercurrent){
    console.log("getallmeapuntolive => usercurrent = ");
    console.log(usercurrent);
    console.log(usercurrent.id);

    return new Promise((resolve, reject) => {
      let partidosParse = Parse.Object.extend("Partido");
      let partidosparse = new Parse.Query(partidosParse);
      partidosparse.include('tipo');
      partidosparse.include('jugador_1');
      partidosparse.include('jugador_2');
      partidosparse.include('jugador_3');
      partidosparse.include('jugador_4');
      partidosparse.include('club');
      partidosparse.include('evento');
      partidosparse.include('owner');
      partidosparse.equalTo("owner", usercurrent.toPointer());
      //tipopedidoparse.equalTo();
      partidosparse.find( {
        success: function(partidosparse) {
          // Execute any logic that should take place after the object is saved.
          console.log(' getallmypartidos => success = ');
          console.log(partidosparse);
          // console.log(contactosparse[0]);
          // console.log(contactosparse.length);
          // console.log(contactosparse[0].relation('contactos'));

          if (partidosparse.length > 0 ) {

            this._partidos = [];
            for (let partido of partidosparse) {
              console.log(partido);
              this._partidos.push(partido);
              //this.contactos.push(contacto);
            }
            this.partidos = this._partidos;

            resolve(this.partidos);

          } else {

            //this.subjectcontactos.next(this.contactos);
            resolve(this.partidos);
          }


        },
        error: function(contactosparse, error) {
          // Execute any logic that should take place if the save fails.
          // error is a Parse.Error with an error code and message.
          console.log(contactosparse);
          console.log(error);
          //this.subjectcontactos.next(this.contactos);
          //alert('Failed to create new object, with error code: ' + error.message);
          reject(error);
        }
      });
    });

  }
  */

  public getsessionToken() {
    let usercurrent = Parse.User.current();
    console.log(usercurrent);
    if(usercurrent){
      console.log(usercurrent.getSessionToken());

      let _session = usercurrent.getSessionToken(); //usercurrent.getSessionToken();
      console.log("Parse.User.getSessionToken()");
      console.log(_session);
      this.parseSession = _session;  //usercurrent['sessionToken'];
      this.createLiveQuery(usercurrent);

    } else {
      // Esto se cumple cuando user no esta logueado.

    }
  }

  private createLiveQuery(usercurrent: any){
    // aqui se inicia la LiveQuery
    this.LiveQueryClient = Parse.LiveQueryClient;
    this.client = new this.LiveQueryClient({
      applicationId: 'myAppId_6666',
      serverURL: 'ws://parse4padel.herokuapp.com/parse',
      javascriptKey: '99b0f6086a032',
      masterKey:  '' //'myMasterKey_6666'
    });


    this.startControl()
      .subscribe( (result) =>{
        console.log(`Meapuntolive => startControl => ${JSON.stringify(result)}`)
      });

    console.log("Meapuntolive => client.open() launch");
    this.client.open(); // conectamos con LIVEQUERY mediante WebSocket

    let query_1 = new Parse.Query('Partido');
    query_1.include("owner", "tipo", "club", "evento", "jugador_1", "jugador_2", "jugador_3", "jugador_4").ascending('Fecha');
    query_1.notEqualTo("jugador_1", usercurrent);
    query_1.notEqualTo("jugador_2", usercurrent);
    query_1.notEqualTo("jugador_3", usercurrent);
    query_1.notEqualTo("jugador_4", usercurrent);
    this.query = query_1;

    console.log(` Meapuntolive Creamos query => ${JSON.stringify(this.query)}` );

    this.getMeapuntolive().subscribe( meapuntolive => {
      console.log("ParselivequeryService - getMeapuntolive().subscribe => this.modeloMeapuntolive");
      this._meapuntolive = [...meapuntolive];
      this.meapuntolive = this._meapuntolive;
      this.subjectmeapuntolive.next(this.meapuntolive); // TODO
      console.log(this.meapuntolive);
    });

    let newsubscription = this.newsSubscription();

    console.log(` Meapuntolive newsubscription => ${JSON.stringify(newsubscription)}`);
    console.log(newsubscription);
    console.log(` Meapuntolive newsubscription => ${JSON.stringify(this.subscription)}`);
    console.log(this.subscription);
    //this.subscription = this.client.subscribe(this.query, this.parseSession);

    this.startUpdate()
      .subscribe( (result) =>{
        console.log(`Meapuntolive => startUpdate => result: ${JSON.stringify(result)}`)

      });
  }

  public newsSubscription() {
    console.log(`newsSubscription 1 pedidosquery? => ${JSON.stringify(this.query)}` );
    if (!this.query) {
      //this.pedidosquery = new Parse.Query('Pedido');
      //console.log(`newsSubscription 2 pedidosquery? => ${JSON.stringify(this.pedidosquery)}` );
      //this.loadPedidos();
    }
    // this.pedidosQuery.equalTo('title', 'broadcast');
    //console.log(`newsSubscription 3 pedidosquery? => ${JSON.stringify(this.query)}` );
    this.subscription = this.client.subscribe(this.query,this.parseSession);
    return this.subscription
  }

  public startUpdate(): Observable<any> {
    console.log(` Meapuntolive call startUpdate() ` );
    return new Observable( observer => {

      // CREATE
      this.subscription.on('create', (meapuntolive) => {
        console.log(` Meapuntolive call this.subscription.on(create) ` );
        console.log(meapuntolive);
        console.log(`meapuntolive create => ${JSON.stringify(meapuntolive)}` );
        this._meapuntolive = [...this.meapuntolive,meapuntolive];
        //let _arraymeapuntolive = [...meapuntolive];
        //this.meapuntolive = this._meapuntolive.concat(_arraymeapuntolive);
        console.log(this._meapuntolive);
        this.subjectmeapuntolive.next(this.meapuntolive);
        observer.next(this.meapuntolive);
      });

      // UPDATE
      this.subscription.on('update', (meapuntolive) => {
        console.log(` Meapuntolive call this.subscription.on(update) ` );
        console.log(meapuntolive);
        console.log(meapuntolive.id);
        console.log(meapuntolive['objectId']);
        //let test = this.modelopedidos.filter(pedido => pedido.objectId == pedidos.get("objectId"));
        //console.log(test);

        this._meapuntolive = [...this.meapuntolive];
        if (meapuntolive.length == this._meapuntolive.length) {
           this.meapuntolive = [...meapuntolive];
        } else {
          console.log("UPDATE MEAPUNTOLIVE NO DEVUELVE TODOS LOS REGISTROS");
          let array = this._meapuntolive.filter((element,index,array)=>{
            //console.log("test array.filter");
            //console.log(element);
            //console.log(index);
            //console.log(array);
            //console.log(meapuntolive.id);
            if (element.id != meapuntolive.id) {
              return element;
            } else {
              return meapuntolive;
            }
          });
          //console.log("array result");
          //console.log(array);
          this.meapuntolive = [...array];
        }
        this.subjectmeapuntolive.next(this.meapuntolive);
        observer.next(this.meapuntolive);
      });

      // CLOSE
      this.subscription.on('close', () => {
        console.log(` Meapuntolive call this.subscription.on(close) ` );
        observer.next();
      });

      // DELETE
      this.subscription.on('delete', (meapuntolive) => {
        console.log(` Meapuntolive call this.subscription.on(delete) ` );
        observer.next(meapuntolive);
        this._meapuntolive = [...this.meapuntolive];
        let array = this._meapuntolive.filter((element,index,array)=>{
            //console.log("test array.filter");
            //console.log(element);
            //console.log(index);
            //console.log(array);
            //console.log(meapuntolive.id);
            if (element.id != meapuntolive.id) {
              return element;
            }
        });
        //console.log("array result");
        //console.log(array);
        this.meapuntolive = [...array];
        this.subjectmeapuntolive.next(this.meapuntolive);
        observer.next(this.meapuntolive);

      });

      // LEAVE
      this.subscription.on('leave', (meapuntolive) => {
        console.log(` Meapuntolive call this.subscription.on(leave) ` );
        observer.next(meapuntolive);
      });

      // ENTER
      this.subscription.on('enter', (meapuntolive) => {
        console.log(` Meapuntolive call this.subscription.on(enter) ` );
        observer.next(meapuntolive);
      });

      // OPEN
      this.subscription.on('open', () => {
        console.log(` Meapuntolive call this.subscription.on(open) ` );
        observer.next();
      });

    })
  }

  public startControl(): Observable<string> {
    console.log(`Meapuntolive call startControl() ` );
    return new Observable<string>( observer => {
      this.client.on('open', () => {
        console.log(`Meapuntolive call this.client.on(open) ` );
        observer.next('open');
      });
      this.client.on('close', () => {
        console.log(`Meapuntolive call this.client.on(close) ` );
        observer.next('close');
      });
      this.client.on('error', () => {
        console.log(`Meapuntolive call this.client.on(error) ` );
        observer.next('error');
      });
      // TODO: other events
      // this.subscription.on('update', (news) => {
      //   this.zone.run(()=> {
      //     this.title = news.get('message')
      //   })
      // })
    })
  }

  public stopUpdate() {
    this.subscription.unsubscribe();
    this.client.close();
  }

  public getMeapuntolive(){
    console.log("getMeapuntolive() ");

    return this.loadMeapuntolive();


    /*
     this.loadPedidos().subscribe(success => {
     console.log("getPedidos().subscribe.success => ");
     console.log(success);
     }, error => {
     console.log("getPedidos().subscribe.error => ");
     console.log(error);
     });
     */
    /*
     if(this.modelopedidos != null)
     {
     console.log("getPedidos => this.modelopedidos != null");
     //return Observable.of(this.modelopedidos);
     }
     else
     {
     console.log("getPedidos => this.modelopedidos = null");
     this.loadPedidos().subscribe(success => {
     console.log("getPedidos().subscribe.success => ");
     console.log(success);
     }, error => {
     console.log("getPedidos().subscribe.error => ");
     console.log(error);
     });
     //return Observable.of(this.modelopedidos);
     }
     */

  }

  public loadMeapuntolive(): Observable<any> {
    return new Observable( observer => {
      this.query.find().then(function (results) {
        console.log('loadMeapuntolive() : results => ' + JSON.stringify(results));
        /*
         console.log(results.length);
         let arraypedidos = [];
         for (let item of results){
         let pedido: Pedido = {
         objectId: item['id'],
         fecha: item.get('createdAt'),
         userpointer: item.get('pedidoUser'),
         videopointer: item.get('pedidoVideo'),
         analisispointer: item.get('pedidoAnalisis'),
         tipopointer: item.get('pedidoTipo'),
         pagado: item.get('pagado'),
         pagopointer: item.get('pedidoPago'),
         estadopointer: item.get('pedidoEstado'),
         observaciones: item.get('Observaciones')
         };
         //console.log(pedido);
         arraypedidos.push(pedido) ;

         }
         //console.log("arraypedidos");
         //console.log(arraypedidos);
         observer.next(arraypedidos);
         */
        observer.next(results);
      }, function (error) {
        console.log('loadMeapuntolive() : error => ' + JSON.stringify(error));
        observer.error(error)

      });
    });

  }




}
