import {Injectable} from "@angular/core";
import {IChatRoom} from "../models/chat-room.model";
import PouchDB from 'pouchdb';
import cordovaSqlitePlugin from 'pouchdb-adapter-cordova-sqlite';

import {PARSE_APP_ID, PARSE_JS_KEY, PARSE_SERVER_URL, GOOGLE_ANALYTICS} from "../config";

declare var Parse: any;

@Injectable()
export class ChatroomsProvider {

  db: any;
  data: any[] = [];

  private _fields = [
    //'users',
    //'profiles',
    //'messages',
  ];

  private _ParseObject: any = Parse.Object.extend('ChatRooms', {});

  constructor() {
    Parse.initialize(PARSE_APP_ID, PARSE_JS_KEY);
    Parse.serverURL = PARSE_SERVER_URL;

    PouchDB.plugin(cordovaSqlitePlugin);
//    this.db = new PouchDB('ChatRooms', {auto_compaction: true, adapter: 'cordova-sqlite'});
    this.db = new PouchDB('ChatRooms', {auto_compaction: true});

    this._fields.map(field => {
      Object.defineProperty(this._ParseObject.prototype, field, {
        get: function () {return this.get(field)},
        set: function (value) { this.set(field, value)}
      });
    });

    // This is a GeoPoint Object
    Object.defineProperty(this._ParseObject.prototype, 'location', {
      get: function () {return this.get('location');},
      set: function (val) {
        this.set('location', new Parse.GeoPoint({
          latitude : val.latitude,
          longitude: val.longitude
        }));
      }
    });
  }

  find(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.cleanDB()
        .then( //() => Parse.Cloud.run('getChatRooms'))
          () => this.getChatRooms())
        .then(data => {
          console.log("DATA");
          console.log(data);
          if(data) {
            data.map(item => this.db.put(item))
          }
        })
        .then(() => this.findCache())
        .then(resolve)
        .catch(reject);
    });
  }

  getChatRooms(): Promise<any> {
    console.log("Chatrooms Provider - getChatRooms()");
    return new Promise((resolve, reject) => {
      let chatRoom = Parse.Object.extend("ChatRooms");
      let chatroom = new Parse.Query(chatRoom);
      console.log(Parse.User.current());
      let current = Parse.User.current();
      let session = current.getSessionToken();
      console.log("Ok Parse.Session");
      console.log(session);
      chatroom.find({
        success: function (chatroom) {
          console.log("Get ChatRooms");
          console.log(chatroom);
          resolve(chatroom);
        },
        error: function ( error) {
          console.log("Error get ChatRooms");
          console.log(error);
          reject(error);
        }
      })
    });
  }

  cleanDB(): Promise<any> {
    this.data = [];
    return new Promise((resolve, reject) => {
      this.db
        .allDocs({include_docs: true})
        .then(result => Promise.all(result.rows.map(row => this.db.remove(row.doc))))
        .then(resolve)
        .catch(reject);
    });
  }


  findCache(): Promise<any> {
    return new Promise(resolve => {
      if (this.data.length > 0) {
        resolve(this.data)
      } else {
        this.db.allDocs({include_docs: true}).then(data => {
          if (data.total_rows) {
            this.data = data.rows.map(row => row.doc);
          }
          resolve(this.data);
        })
      }
    });
  }

  getCache(id: string) {
    return this.db.get(id);
  }

  // Parse Crud
  get(objectId: string) {
    return new Parse.Query(this._ParseObject).get(objectId);
  }

  create(params: {users: any[], message?: string}): Promise<any> {
    return new Parse.Cloud.run('createChatRooms', params)
  }

  put(item: IChatRoom) {

    if (!item.id) {
      // New Item
      return new this._ParseObject().save(item);
    } else {
      // Update item
      return item.save();
    }

  }

  destroy(objectId: string) {
    return new Promise((resolve, reject) => {
      this.get(objectId).then(item => {
        item.destroy().then(resolve).catch(reject);
      }).catch(reject);
    });
  }
}
