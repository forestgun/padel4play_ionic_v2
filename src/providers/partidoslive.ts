import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Rx';

import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { UserProvider } from './user';

import {PARSE_APP_ID, PARSE_JS_KEY, PARSE_SERVER_URL, GOOGLE_ANALYTICS} from "../config";

import * as Parse from 'parse';
/*
  Generated class for the Partidoslive provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class Partidoslive {

  public partidoslive: Array<any>;
  private _partidoslive: Array<any>;

  client: any;
  LiveQueryClient: any;
  parseSession: any;
  subscription: any;
  query: any;


  public subjectpartidoslive = new BehaviorSubject(null);


  constructor(public http: Http,
              private userprovider: UserProvider
  ) {
    console.log('Hello partidoslive Provider');
    Parse.initialize(PARSE_APP_ID, PARSE_JS_KEY);
    Parse.serverURL = PARSE_SERVER_URL;

    this.partidoslive = [];  // usar en formularios
    this._partidoslive = [];

    this.parseSession = null;
    this.subscription = null;
    this.query = null;

    this.subjectpartidoslive.next(this.partidoslive);

    this.getsessionToken();

  }


  // GET -- PARTIDOS
  /*
   public getallpartidoslive(usercurrent){
   console.log("getallpartidoslive => usercurrent = ");
   console.log(usercurrent);
   console.log(usercurrent.id);

   return new Promise((resolve, reject) => {
   let partidosParse = Parse.Object.extend("Partido");
   let partidosparse = new Parse.Query(partidosParse);
   partidosparse.include('tipo');
   partidosparse.include('jugador_1');
   partidosparse.include('jugador_2');
   partidosparse.include('jugador_3');
   partidosparse.include('jugador_4');
   partidosparse.include('club');
   partidosparse.include('evento');
   partidosparse.include('owner');
   partidosparse.equalTo("owner", usercurrent.toPointer());
   //tipopedidoparse.equalTo();
   partidosparse.find( {
   success: function(partidosparse) {
   // Execute any logic that should take place after the object is saved.
   console.log(' getallmypartidos => success = ');
   console.log(partidosparse);
   // console.log(contactosparse[0]);
   // console.log(contactosparse.length);
   // console.log(contactosparse[0].relation('contactos'));

   if (partidosparse.length > 0 ) {

   this._partidos = [];
   for (let partido of partidosparse) {
   console.log(partido);
   this._partidos.push(partido);
   //this.contactos.push(contacto);
   }
   this.partidos = this._partidos;

   resolve(this.partidos);

   } else {

   //this.subjectcontactos.next(this.contactos);
   resolve(this.partidos);
   }


   },
   error: function(contactosparse, error) {
   // Execute any logic that should take place if the save fails.
   // error is a Parse.Error with an error code and message.
   console.log(contactosparse);
   console.log(error);
   //this.subjectcontactos.next(this.contactos);
   //alert('Failed to create new object, with error code: ' + error.message);
   reject(error);
   }
   });
   });

   }
   */

  public getsessionToken() {
    let usercurrent = Parse.User.current();
    console.log(usercurrent);
    if(usercurrent){
      console.log(usercurrent.getSessionToken());

      let _session = usercurrent.getSessionToken(); //usercurrent.getSessionToken();
      console.log("Parse.User.getSessionToken()");
      console.log(_session);
      this.parseSession = _session;  //usercurrent['sessionToken'];
      this.createLiveQuery(usercurrent);

    } else {
      // Esto se cumple cuando user no esta logueado.

    }

  }



  private createLiveQuery(usercurrent: any) {
    // aqui se inicia la LiveQuery
    this.LiveQueryClient = Parse.LiveQueryClient;
    this.client = new this.LiveQueryClient({
      applicationId: 'myAppId_6666',
      serverURL: 'ws://parse4padel.herokuapp.com/parse',
      javascriptKey: '99b0f6086a032',
      masterKey:  '' //'myMasterKey_6666'
    });


    this.startControl()
      .subscribe( (result) =>{
        console.log(`partidoslive => startControl => ${JSON.stringify(result)}`)
      });

    console.log("partidoslive => client.open() launch");
    this.client.open(); // conectamos con LIVEQUERY mediante WebSocket


    let query_1 = new Parse.Query('Partido');
    let query_3 = new Parse.Query('Partido');
    let query_2 = new Parse.Query('Partido');
    let query_4 = new Parse.Query('Partido');
    query_1.include("owner", "tipo", "club", "evento", "jugador_1", "jugador_2", "jugador_3", "jugador_4").ascending('Fecha');
    query_1.equalTo("jugador_1", usercurrent);
    query_2.include("owner", "tipo", "club", "evento", "jugador_1", "jugador_2", "jugador_3", "jugador_4").ascending('Fecha');
    query_2.equalTo("jugador_2", usercurrent);
    query_3.include("owner", "tipo", "club", "evento", "jugador_1", "jugador_2", "jugador_3", "jugador_4").ascending('Fecha');
    query_3.equalTo("jugador_3", usercurrent);
    query_4.include("owner", "tipo", "club", "evento", "jugador_1", "jugador_2", "jugador_3", "jugador_4").ascending('Fecha');
    query_4.equalTo("jugador_4", usercurrent);
    this.query = Parse.Query.or(query_1,query_2,query_3,query_4);


    console.log(` partidoslive Creamos query => ${JSON.stringify(this.query)}` );

    this.getpartidoslive().subscribe( partidoslive => {
      console.log("ParselivequeryService - getpartidoslive().subscribe => this.modelopartidoslive");
      this._partidoslive = [...partidoslive];
      this.partidoslive = this._partidoslive;
      this.subjectpartidoslive.next(this.partidoslive); // TODO
      console.log(this.partidoslive);
    });

    let newsubscription = this.newsSubscription();

    console.log(` partidoslive newsubscription => ${JSON.stringify(newsubscription)}`);
    console.log(newsubscription);
    console.log(` partidoslive newsubscription => ${JSON.stringify(this.subscription)}`);
    console.log(this.subscription);
    //this.subscription = this.client.subscribe(this.query, this.parseSession);

    this.startUpdate()
      .subscribe( (result) =>{
        console.log(`partidoslive => startUpdate => result: ${JSON.stringify(result)}`)

      });
  }

  public newsSubscription() {
    console.log(`newsSubscription 1 pedidosquery? => ${JSON.stringify(this.query)}` );
    if (!this.query) {
      //this.pedidosquery = new Parse.Query('Pedido');
      //console.log(`newsSubscription 2 pedidosquery? => ${JSON.stringify(this.pedidosquery)}` );
      //this.loadPedidos();
    }
    // this.pedidosQuery.equalTo('title', 'broadcast');
    //console.log(`newsSubscription 3 pedidosquery? => ${JSON.stringify(this.query)}` );
    this.subscription = this.client.subscribe(this.query,this.parseSession);
    return this.subscription
  }

  public startUpdate(): Observable<any> {
    console.log(` partidoslive call startUpdate() ` );
    return new Observable( observer => {

      // CREATE
      this.subscription.on('create', (partidoslive) => {
        console.log(` partidoslive call this.subscription.on(create) ` );
        console.log(partidoslive);
        console.log(`partidoslive create => ${JSON.stringify(partidoslive)}` );
        this._partidoslive = [...this.partidoslive,partidoslive];
        //let _arraypartidoslive = [...partidoslive];
        //this.partidoslive = this._partidoslive.concat(_arraypartidoslive);
        console.log(this._partidoslive);
        this.subjectpartidoslive.next(this.partidoslive);
        observer.next(this.partidoslive);
      });

      // UPDATE
      this.subscription.on('update', (partidoslive) => {
        console.log(` partidoslive call this.subscription.on(update) ` );
        console.log(partidoslive);
        console.log(partidoslive.id);
        console.log(partidoslive['objectId']);
        //let test = this.modelopedidos.filter(pedido => pedido.objectId == pedidos.get("objectId"));
        //console.log(test);

        this._partidoslive = [...this.partidoslive];
        if (partidoslive.length == this._partidoslive.length) {
          this.partidoslive = [...partidoslive];
        } else {
          console.log("UPDATE partidoslive NO DEVUELVE TODOS LOS REGISTROS");
          let array = this._partidoslive.filter((element,index,array)=>{
            //console.log("test array.filter");
            //console.log(element);
            //console.log(index);
            //console.log(array);
            //console.log(partidoslive.id);
            if (element.id != partidoslive.id) {
              return element;
            } else {
              return partidoslive;
            }
          });
          //console.log("array result");
          //console.log(array);
          this.partidoslive = [...array];
        }
        this.subjectpartidoslive.next(this.partidoslive);
        observer.next(this.partidoslive);
      });

      // CLOSE
      this.subscription.on('close', () => {
        console.log(` partidoslive call this.subscription.on(close) ` );
        observer.next();
      });

      // DELETE
      this.subscription.on('delete', (partidoslive) => {
        console.log(` partidoslive call this.subscription.on(delete) ` );
        observer.next(partidoslive);
        this._partidoslive = [...this.partidoslive];
        let array = this._partidoslive.filter((element,index,array)=>{
          //console.log("test array.filter");
          //console.log(element);
          //console.log(index);
          //console.log(array);
          //console.log(partidoslive.id);
          if (element.id != partidoslive.id) {
            return element;
          }
        });
        //console.log("array result");
        //console.log(array);
        this.partidoslive = [...array];
        this.subjectpartidoslive.next(this.partidoslive);
        observer.next(this.partidoslive);

      });

      // LEAVE
      this.subscription.on('leave', (partidoslive) => {
        console.log(` partidoslive call this.subscription.on(leave) ` );
        observer.next(partidoslive);
      });

      // ENTER
      this.subscription.on('enter', (partidoslive) => {
        console.log(` partidoslive call this.subscription.on(enter) ` );
        observer.next(partidoslive);
      });

      // OPEN
      this.subscription.on('open', () => {
        console.log(`partidoslive call this.subscription.on(open) ` );
        observer.next();
      });

    })
  }

  public startControl(): Observable<string> {
    console.log(`partidoslive call startControl() ` );
    return new Observable<string>( observer => {
      this.client.on('open', () => {
        console.log(`partidoslive call this.client.on(open) ` );
        observer.next('open');
      });
      this.client.on('close', () => {
        console.log(`partidoslive call this.client.on(close) ` );
        observer.next('close');
      });
      this.client.on('error', () => {
        console.log(`partidoslive call this.client.on(error) ` );
        observer.next('error');
      });
      // TODO: other events
      // this.subscription.on('update', (news) => {
      //   this.zone.run(()=> {
      //     this.title = news.get('message')
      //   })
      // })
    })
  }

  public stopUpdate() {
    this.subscription.unsubscribe();
    this.client.close();
  }


  public getpartidoslive(){
    console.log("getpartidoslive() ");

    return this.loadpartidoslive();


    /*
     this.loadPedidos().subscribe(success => {
     console.log("getPedidos().subscribe.success => ");
     console.log(success);
     }, error => {
     console.log("getPedidos().subscribe.error => ");
     console.log(error);
     });
     */
    /*
     if(this.modelopedidos != null)
     {
     console.log("getPedidos => this.modelopedidos != null");
     //return Observable.of(this.modelopedidos);
     }
     else
     {
     console.log("getPedidos => this.modelopedidos = null");
     this.loadPedidos().subscribe(success => {
     console.log("getPedidos().subscribe.success => ");
     console.log(success);
     }, error => {
     console.log("getPedidos().subscribe.error => ");
     console.log(error);
     });
     //return Observable.of(this.modelopedidos);
     }
     */

  }

  public loadpartidoslive(): Observable<any> {
    return new Observable( observer => {
      this.query.find().then(function (results) {
        console.log('loadpartidoslive() : results => ' + JSON.stringify(results));
        /*
         console.log(results.length);
         let arraypedidos = [];
         for (let item of results){
         let pedido: Pedido = {
         objectId: item['id'],
         fecha: item.get('createdAt'),
         userpointer: item.get('pedidoUser'),
         videopointer: item.get('pedidoVideo'),
         analisispointer: item.get('pedidoAnalisis'),
         tipopointer: item.get('pedidoTipo'),
         pagado: item.get('pagado'),
         pagopointer: item.get('pedidoPago'),
         estadopointer: item.get('pedidoEstado'),
         observaciones: item.get('Observaciones')
         };
         //console.log(pedido);
         arraypedidos.push(pedido) ;

         }
         //console.log("arraypedidos");
         //console.log(arraypedidos);
         observer.next(arraypedidos);
         */
        observer.next(results);
      }, function (error) {
        console.log('loadpartidoslive() : error => ' + JSON.stringify(error));
        observer.error(error)

      });
    });

  }


}
