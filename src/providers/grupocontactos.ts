import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
//import 'rxjs/add/operator/map';
//import {Observable} from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { UserProvider } from './user';
import { GrupocontactosModel } from '../models/grupocontactos.model';

import {PARSE_APP_ID, PARSE_JS_KEY, PARSE_SERVER_URL, GOOGLE_ANALYTICS} from "../config";

import * as Parse from 'parse';

/*
  Generated class for the Grupocontactos provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class Grupocontactos {

  // public estadospedidos: Array<Estadopedido>;
  public gruposcontactos: Array<GrupocontactosModel>;
  private _gruposcontactos: Array<GrupocontactosModel>;

  public subjectgrupocontactos = new BehaviorSubject(null);

  constructor(public http: Http,
              private userprovider: UserProvider
  ) {
    console.log('Hello Grupocontactos Provider');
    Parse.initialize(PARSE_APP_ID, PARSE_JS_KEY);
    Parse.serverURL = PARSE_SERVER_URL;

    this.gruposcontactos = [];  // usar en formularios
    this._gruposcontactos = [];

    this.subjectgrupocontactos.next(this.gruposcontactos);

    let usercurrent = this.userprovider.current;

    // PROMISE -- GRUPOSCONTACTOS
    /*
    this.getallmygruposcontactos(usercurrent).then((result) => {
      console.log("getallmygruposcontactos resuelve con result => ");
      //console.log(result);
      let jsonfile = JSON.stringify(result);
      let object = JSON.parse(jsonfile);
      //console.log(object);
      for (let grupocontacto of object) {
        //this.estadospedidos.push(new Estadopedido(estado.objectId, estado.nombre));
        this.gruposcontactos.push(grupocontacto);
      }
      console.log(this.gruposcontactos);
    }, (error) => {
      console.log("getallmygruposcontactos resuelve con error => ");
      console.error(error);
    });
    */


  }

  // GET -- GRUPOSCONTACTOS
  public getallmygruposcontactos(usercurrent){
    console.log("getallmygruposcontactos => usercurrent = ");
    console.log(usercurrent);
    console.log(usercurrent.id);
    return new Promise((resolve, reject) => {
      let userpointer = usercurrent.toPointer();
      console.log(userpointer);
      let gruposcontactosParse = Parse.Object.extend("GrupoContactos");
      let gruposcontactosparse = new Parse.Query(gruposcontactosParse);
      gruposcontactosparse.include('contactos');
      gruposcontactosparse.include('owner');
      gruposcontactosparse.equalTo("owner", userpointer );
      //tipopedidoparse.equalTo();
      gruposcontactosparse.find( {
        success: function(grupocontactosparse) {
          // Execute any logic that should take place after the object is saved.
          console.log(' getallmycontactos => success = ');
          console.log(grupocontactosparse);
          let arraygrupos = [];
          // console.log(contactosparse[0]);
          // console.log(contactosparse.length);
          // console.log(contactosparse[0].relation('contactos'));
          if (grupocontactosparse.length > 0 ) {

            for (let grupo of grupocontactosparse ) {

              let relation = grupo.relation('contactos');
              relation.query().find({
                success: function(contactos) {
                  console.log("success query relation.contactos in gruposcontactos");
                  console.log(contactos);
                  //let grupomodel = new this.grupocontactosModel(grupo,contactos);
                  let grupomodel = new GrupocontactosModel();

                  grupomodel.setGrupo(grupo);

                  grupomodel.addContactos(contactos);

                  arraygrupos.push(grupomodel);
                },
                error: function(contactos,error) {
                  console.log("error query relation.contactos in gruposcontactos");
                  console.log(error);
                  reject(error);
                }
              });

            }
            console.log("SALIMOS DE GETALLMYGRUPOSCONTACTOS");
            console.log(arraygrupos);
            this.gruposcontactos = arraygrupos;
            resolve(this.gruposcontactos);


          } else {

            resolve(this.gruposcontactos);
          }


        },
        error: function(grupocontactosparse, error) {
          // Execute any logic that should take place if the save fails.
          // error is a Parse.Error with an error code and message.
          console.log(grupocontactosparse);
          console.log(error);
          //alert('Failed to create new object, with error code: ' + error.message);
          reject(error);
        }
      });
    });

  }

  /*
  public getGruposContacts(){
    return Observable.of(this.gruposcontactos);
  }
  */

}
