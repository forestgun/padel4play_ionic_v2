import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
//import 'rxjs/add/operator/map';

import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { UserProvider } from './user';

import {PARSE_APP_ID, PARSE_JS_KEY, PARSE_SERVER_URL, GOOGLE_ANALYTICS} from "../config";

import * as Parse from 'parse';

/*
  Generated class for the Partidos provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class Partidos {

  public partidos: Array<any>;
  private _partidos: Array<any>;

  public subjectpartidos = new BehaviorSubject(null);

  constructor(public http: Http,
              private userprovider: UserProvider
  ) {
    console.log('Hello Partidos Provider');
    Parse.initialize(PARSE_APP_ID, PARSE_JS_KEY);
    Parse.serverURL = PARSE_SERVER_URL;

    this.partidos = [];  // usar en formularios
    this._partidos = [];

    this.subjectpartidos.next(this.partidos);


    let usercurrent = this.userprovider.current;
    console.log("PARTIDOS => usercurrent");
    console.log(usercurrent);



  }


  // GET -- PARTIDOS
  public getallmypartidos(usercurrent){
    console.log("getallmypartidos => usercurrent = ");
    console.log(usercurrent);
    console.log(usercurrent.id);

    return new Promise((resolve, reject) => {
      let partidosParse = Parse.Object.extend("Partido");
      let partidosparse = new Parse.Query(partidosParse);
      partidosparse.include('tipo');
      partidosparse.include('jugador_1');
      partidosparse.include('jugador_2');
      partidosparse.include('jugador_3');
      partidosparse.include('jugador_4');
      partidosparse.include('club');
      partidosparse.include('evento');
      partidosparse.include('owner');
      partidosparse.equalTo("owner", usercurrent.toPointer());
      //tipopedidoparse.equalTo();
      partidosparse.find( {
        success: function(partidosparse) {
          // Execute any logic that should take place after the object is saved.
          console.log(' getallmypartidos => success = ');
          console.log(partidosparse);
          // console.log(contactosparse[0]);
          // console.log(contactosparse.length);
          // console.log(contactosparse[0].relation('contactos'));

          if (partidosparse.length > 0 ) {

                this._partidos = [];
                for (let partido of partidosparse) {
                  console.log(partido);
                  this._partidos.push(partido);
                  //this.contactos.push(contacto);
                }
                this.partidos = this._partidos;

                resolve(this.partidos);

          } else {

            //this.subjectcontactos.next(this.contactos);
            resolve(this.partidos);
          }


        },
        error: function(contactosparse, error) {
          // Execute any logic that should take place if the save fails.
          // error is a Parse.Error with an error code and message.
          console.log(contactosparse);
          console.log(error);
          //this.subjectcontactos.next(this.contactos);
          //alert('Failed to create new object, with error code: ' + error.message);
          reject(error);
        }
      });
    });

  }



}
