import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import * as Parse from 'parse';

/*
  Generated class for the Userclubs provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class Userclubs {

  constructor(public http: Http) {
    console.log('Hello Userclubs Provider');
  }

}
