import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
//import 'rxjs/add/operator/map';
//import {Observable} from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { UserProvider } from './user';

import {PARSE_APP_ID, PARSE_JS_KEY, PARSE_SERVER_URL, GOOGLE_ANALYTICS} from "../config";

import * as Parse from 'parse';

/*
  Generated class for the Contactos provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class Contactos {

  // public estadospedidos: Array<Estadopedido>;
  public contactos: Array<any>;
  private _contactos: Array<any>;

  public subjectcontactos = new BehaviorSubject(null);

  constructor(public http: Http, private userprovider: UserProvider) {
    console.log('Hello Contactos Provider');
    Parse.initialize(PARSE_APP_ID, PARSE_JS_KEY);
    Parse.serverURL = PARSE_SERVER_URL;

    this.contactos = [];  // usar en formularios
    this._contactos = [];

    this.subjectcontactos.next(this.contactos);


    let usercurrent = this.userprovider.current;
    console.log("CONTACTOS => usercurrent");
    console.log(usercurrent);

    // PROMISE -- CONTACTOS
    /*
    this.getallmycontactos(usercurrent).then((result) => {
      console.log("getallmycontactos resuelve con result => ");
      //console.log(result);
      this._contactos = [];
      let jsonfile = JSON.stringify(result);
      let object = JSON.parse(jsonfile);
      //console.log(object);
      for (let contacto of object) {
        //this.estadospedidos.push(new Estadopedido(estado.objectId, estado.nombre));
        this._contactos.push(contacto);
      }
      console.log(this._contactos);
      this.contactos = this._contactos;
    }, (error) => {
      console.log("getallmycontactos resuelve con error => ");
      console.error(error);
    });
    */


  }

  // GET -- CONTACTOS
  public getallmycontactos(usercurrent){
    console.log("getallmycontactos => usercurrent = ");
    console.log(usercurrent);
    console.log(usercurrent.id);

    return new Promise((resolve, reject) => {
      let contactosParse = Parse.Object.extend("Contactos");
      let contactosparse = new Parse.Query(contactosParse);
      contactosparse.include('contactos');
      contactosparse.include('owner');
      contactosparse.equalTo("owner", usercurrent.toPointer());
      //tipopedidoparse.equalTo();
      contactosparse.find( {
        success: function(contactosparse) {
          // Execute any logic that should take place after the object is saved.
          console.log(' getallmycontactos => success = ');
          console.log(contactosparse);
          // console.log(contactosparse[0]);
          // console.log(contactosparse.length);
          // console.log(contactosparse[0].relation('contactos'));
          if (contactosparse.length > 0 ) {
            let relation = contactosparse[0].relation('contactos');
            relation.query().find({
              success: function(contactos) {
                console.log("success query relation.contactos");
                console.log(contactos);
                this._contactos = [];
                for (let contacto of contactos) {
                  console.log(contacto);
                  this._contactos.push(contacto);
                  //this.contactos.push(contacto);

                }
                this.contactos = this._contactos;
                console.log("CONTACTOS => salimos del bucle y vamos a next");
                console.log(this.subjectcontactos);

                resolve(this.contactos);
              },
              error: function(contactos,error) {
                console.log("error query relation.contactos");
                console.log(error);
               reject(error);
              }
            });

          } else {

            //this.subjectcontactos.next(this.contactos);
            resolve(contactosparse);
          }


        },
        error: function(contactosparse, error) {
          // Execute any logic that should take place if the save fails.
          // error is a Parse.Error with an error code and message.
          console.log(contactosparse);
          console.log(error);
          //this.subjectcontactos.next(this.contactos);
          //alert('Failed to create new object, with error code: ' + error.message);
          reject(error);
        }
      });
    });

  }

  /*
  public getContacts(){
    console.log("getContacts");
    console.log(this.contactos);

    return Observable.of(this.contactos);
  }
  */

}
