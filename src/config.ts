// AppName
export const APP_NAME: string = 'Padel4Play';

// Parse
export const PARSE_APP_ID: string     = 'myAppId_6666';
export const PARSE_JS_KEY: string     = '99b0f6086a032';

//export const PARSE_SERVER_URL: string = 'http://localhost:1337/parse/';
export const PARSE_SERVER_URL: string = 'https://parse4padel.herokuapp.com/parse/';

// Google Maps
export const GOOGLE_MAPS_WEB: string = 'AIzaSyBDzKoOhW8bduh8-XrwDZBo1n2IFd5gqbc';

// Google Analytics
export const GOOGLE_ANALYTICS: string = 'UA-92020323-1';

// Facebook
export const facebook_appId: string      = '1226156847461723';
export const facebook_appVersion: string = 'v2.8';

// Languages
export const languages: [any]         = [
    {
        name: 'English',
        code: 'en',
        flag: 'en'
    },
    {
        name: 'Portugues',
        code: 'pt',
        flag: 'br'
    },
    {
        name: 'German',
        code: 'de',
        flag: 'de'
    },
    {
        name: 'French',
        code: 'fr',
        flag: 'fr'
    },
    {
        name: 'Greek',
        code: 'el',
        flag: 'el'
    },
    {
        name: 'Spanish',
        code: 'es',
        flag: 'es'
    },
    {
        name: 'Japanese',
        code: 'ja',
        flag: 'ja'
    },
    {
        name: 'Chinese',
        code: 'zh',
        flag: 'zh'
    },
    {
        name: 'Russian',
        code: 'ru',
        flag: 'ru'
    },
    {
        name: 'Bengali (Bangla)',
        code: 'bn',
        flag: 'bn'
    },
    {
        name: 'Turkish',
        code: 'tr',
        flag: 'tr'
    },

];
export const language_default: string = 'en';
