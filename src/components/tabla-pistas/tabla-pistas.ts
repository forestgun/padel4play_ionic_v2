import {Component, EventEmitter, Input, Output, OnInit} from '@angular/core';

@Component({
  selector   : 'tabla-pistas',
  templateUrl: 'tabla-pistas.html'
})
export class TablaPistasComponent implements OnInit {

  @Input('idresource') idresource: string;
  @Input('resourcename') resourcename: string;
  @Input('horas') horas: any[];

  @Output() horaselect = new EventEmitter();

  constructor(){
    console.log('Hello TablaPistas Component');
    // aqui los Input aun esta sin definir y no tiene valor
    // console.log(this.idresource);
    // console.log(this.resourcename);
    // console.log(this.horas);

  }

  ngOnInit(){
    //called after the constructor and called  after the first ngOnChanges()
    console.log('Hello TablaPistas Component  ngOnInit ');
    // aqui los Input ya tienen valores y estan definidos
    console.log(this.idresource);
    console.log(this.resourcename);
    console.log(this.horas);
  }

  selectHoraPista(idpista: string, hora:string, nombrepista: string){
    console.log("click selectHoraPista()");
    console.log(idpista);
    console.log(hora);
    console.log(nombrepista)
    this.horaselect.emit({idresource: idpista, hora: hora, resourcename: nombrepista});
  }
}
